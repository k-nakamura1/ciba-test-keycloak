module Api
  module V1
    class AuthController < ApplicationController
      def delegation
        alert = {
          title: 'ユーザーの購入の許可の確認について',
          subtitle: "#{params[:login_hint]} さんの A Shop で購入の許可の申請",
          body: "識別コードは #{params[:binding_message]} です。"
        }
 
        path = Rails.root.join('tmp/apns')

        File.open("#{path}/test.apns", 'w') do |file|
          hash = { 
            aps: {
              alert: alert,
              sound: 'default',
              badge: 1,
              category: 'TEST_CATEGORY',
            },
            PUSH_TEST_MESSAGE_ID: '0000000001',
            AUTHORIZATION: request.headers['Authorization'],
          } 
          JSON.dump(hash, file)
        end
        
        head(201)
      end
    end
  end
end
