## Setup
```
user@host: ~/workspace $ docker compose up -d
```

## Check Keycloak
```
user@host: ~/workspace $ open http://localhost:8088/
```
